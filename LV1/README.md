# Robert Marković i Nikola Pavković - Web programiranje - Projekt
## Osnovna ideja web-aplikacije
Web aplikacija generira nasumične riječi ovisno o korisnički definiranim filterima.
Korisnik definira **broj, tip i kategoriju** riječi koje će se generirati.
Web aplikacija nudi tri različite mogućnosti:<br><br>
**generiranje imena benda**<br>
**generiranje dječjeg imena**<br>
**generiranje imena psa**<br>

# Korištene tehnologije
## Backend
Koristit će se Java Spring Boot za definiranje pozadinskog rada aplikacije.

## Frontend
## Baza podataka
Za bazu podataka koristit će se Firebase Firestore NoSQL baza podataka.
